﻿Shader "Dither/GetOffsets"
{
	Properties
	{
		_MainTex ("Camera Capture", 2D) = "white" {}
		_RendTex ("Rendered Texture", 2D) = "grey" {}
		_OffsTex ("Last Offsets", 2D) = "Grey" {}
		
		_fadeRate ("Fade Rate", Range(0.0,1.0)) = .1
		_fadeToGreyRate ("Fade to grey rate", Range(0.0,1.0)) = .01
		_thisPixMult ("This pixel weight", float) = 4
		_taxiPixMult ("neighbor pixel weight", float) = 4
		_cornerPixMult ("corner pixel weight", float) = 4
		
		_boundary ("Boundary", Range(-1.0,1.0)) = 0.0
		
		_black ("Black", Color) = (.1,.3,.2,1)
		_white ("White", Color) = (.8,1,.9,1)
		_actuallyGreyTho ("Grey", Color) = (.5,.5,.5,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			float rand(float2 co)
			 {
					 return frac((sin( dot(co.xy , float2(12.345 * _Time.w, 67.890 * _Time.w) )) * 12345.67890+_Time.w));
			 }

			float sumup(fixed4 col){
				 return (col.x + col.y + col.z)/3.0;
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			sampler2D _RendTex;
			sampler2D _OffsTex;
			fixed4 _actuallyGreyTho;
			fixed4 _black;
			fixed4 _white;
			
			float _fadeRate;
			float _fadeToGreyRate;
			float _boundary;

			float _thisPixMult;
			float _taxiPixMult;
			float _cornerPixMult;


			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_OffsTex, i.uv);

				 float errorx = (
				   (tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).rgb.x - tex2D(_RendTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).rgb.x) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).rgb.x - tex2D(_RendTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).rgb.x) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).rgb.x - tex2D(_RendTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).rgb.x) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).rgb.x - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).rgb.x) * _taxiPixMult

				 
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.x - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.x) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.x - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.x) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.x - tex2D(_RendTex, i.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.x) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.x - tex2D(_RendTex, i.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.x) * _cornerPixMult

				 + (tex2D(_MainTex, i.uv).rgb.x - tex2D(_RendTex, i.uv).rgb.x)*_thisPixMult
				 )/(_thisPixMult + (_taxiPixMult * 4.0) + (_cornerPixMult * 4.0));

				 float errory = (
				   (tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).rgb.y - tex2D(_RendTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).rgb.y) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).rgb.y - tex2D(_RendTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).rgb.y) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).rgb.y - tex2D(_RendTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).rgb.y) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).rgb.y - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).rgb.y) * _taxiPixMult

				 
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.y - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.y) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.y - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.y) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.y - tex2D(_RendTex, i.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.y) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.y - tex2D(_RendTex, i.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.y) * _cornerPixMult

				 + (tex2D(_MainTex, i.uv).rgb.y - tex2D(_RendTex, i.uv).rgb.y)*_thisPixMult
				 )/(_thisPixMult + (_taxiPixMult * 4.0) + (_cornerPixMult * 4.0));

				 				 float errorz = (
				   (tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).rgb.z - tex2D(_RendTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).rgb.z) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).rgb.z - tex2D(_RendTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).rgb.z) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).rgb.z - tex2D(_RendTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).rgb.z) * _taxiPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).rgb.z - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).rgb.z) * _taxiPixMult

				 
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.z - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.z) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.z - tex2D(_RendTex, i.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.z) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.z - tex2D(_RendTex, i.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y)).rgb.z) * _cornerPixMult
				 + (tex2D(_MainTex, i.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.z - tex2D(_RendTex, i.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y)).rgb.z) * _cornerPixMult

				 + (tex2D(_MainTex, i.uv).rgb.z - tex2D(_RendTex, i.uv).rgb.z)*_thisPixMult
				 )/(_thisPixMult + (_taxiPixMult * 4.0) + (_cornerPixMult * 4.0));

				//float error = sumup(tex2D(_MainTex, i.uv)) - sumup(tex2D(_RendTex, i.uv));
				
				
				col = lerp(col, _actuallyGreyTho, rand(i.uv)*_fadeToGreyRate);
				//if( tex2D(_MainTex, i.uv).rgb.x - tex2D(_RendTex, i.uv).rgb.x < -0.5)
				
				float x = col.rgb.x;
				if( errorx > _boundary )
				//if(rand(i.uv)<0.5)
				{
					if(rand(i.uv) < _fadeRate)
					x = lerp(x,_white.rgb.x,rand(i.uv+1));
					//col = lerp(col, _white, rand(i.uv)*_fadeRate);
					//col = lerp(col, _white, rand(i.uv+1));
					
					//col = lerp(col, _white, _fadeRate);
				} else {
					if(rand(i.uv) < _fadeRate)
					//col = lerp(col, _black, rand(i.uv)*_fadeRate);
					x = lerp(x, _black.rgb.x, rand(i.uv+1 ));
					//col = lerp(col, _black, _fadeRate);
				}

				float y = col.rgb.y;
				if( errory > _boundary )
				//if(rand(i.uv)<0.5)
				{
					if(rand(i.uv) < _fadeRate)
					y = lerp(y,_white.rgb.y,rand(i.uv+1));
					//col = lerp(col, _white, rand(i.uv)*_fadeRate);
					//col = lerp(col, _white, rand(i.uv+1));
					
					//col = lerp(col, _white, _fadeRate);
				} else {
					if(rand(i.uv) < _fadeRate)
					//col = lerp(col, _black, rand(i.uv)*_fadeRate);
					y = lerp(y, _black.rgb.y, rand(i.uv+1 ));
					//col = lerp(col, _black, _fadeRate);
				}

				float z = col.rgb.z;
				if( errorz > _boundary )
				//if(rand(i.uv)<0.5)
				{
					if(rand(i.uv) < _fadeRate)
					z = lerp(z,_white.rgb.z,rand(i.uv+1));
					//col = lerp(col, _white, rand(i.uv)*_fadeRate);
					//col = lerp(col, _white, rand(i.uv+1));
					
					//col = lerp(col, _white, _fadeRate);
				} else {
					if(rand(i.uv) < _fadeRate)
					//col = lerp(col, _black, rand(i.uv)*_fadeRate);
					z = lerp(z, _black.rgb.z, rand(i.uv+1 ));
					//col = lerp(col, _black, _fadeRate);
				}

				col = fixed4(x,y,z,1.0);

				//if (sumup(col) == sumup(_white) || sumup(col) == sumup(_black)){
				//	col = _actuallyGreyTho;
				//}
				//col = lerp(_black, _white, error);

				//col = tex2D(_RendTex, i.uv);

				//col.rgb = ( -error / 5.0 ) + _actuallyGreyTho;//real correction??
				//col.rgb = _actuallyGreyTho;//no correction
				//col.rgb = (i.uv.x + i.uv.y) * 10 % 1;

				return col;
			}
			ENDCG
		}
	}
}
