﻿Shader "Dither/dither"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_OffsetsTex ("Offsets Texture", 2D) = "Grey" {}
		_dithHeight ("Dither height", Int) = 255
		_boundary ("Boundary", Range(0.0,1.0)) = .5
		_fadeRate ("Fade Rate", Range(0.0,1.0)) = .9

		_black ("Black", Color) = (.1,.3,.2,1)
		_white ("White", Color) = (.8,1,.9,1)
		_actuallyGreyTho ("Grey", Color) = (.5,.5,.5,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			float rand(float2 co)
             {
                     return frac((sin( dot(co.xy , float2(12.345 * _Time.w, 67.890 * _Time.w) )) * 12345.67890+_Time.w));
             }


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _OffsetsTex;
			float _boundary;
			float _fadeRate;
			fixed4 _black;
			fixed4 _white;
			fixed4 _actuallyGreyTho;

			fixed4 frag (v2f i) : SV_Target
			{
				//fixed4 col = tex2D(_OffsetsTex, i.uv);
				//fixed4 realCol = tex2D(_MainTex, i.uv);
				//if((col.rgb.x + col.rgb.y + col.rgb.z)  < (realCol.rgb.x + realCol.rgb.y + realCol.rgb.z))
				//{
				//	col = lerp(col, _white, rand(i.uv)*_fadeRate);
				//}
				//else {
				//	col = lerp(col, _black, rand(i.uv)*_fadeRate) ;
				//}
				fixed4 col = tex2D(_MainTex, i.uv)  + tex2D(_OffsetsTex, i.uv) - _actuallyGreyTho;
				//col =  tex2D(_OffsetsTex, i.uv);
				// just invert the colors
				if(col.rgb.x < _boundary){
					col.rgb.x = _black.x;
				} else {
					col.rgb.x = _white.x;
				}
				if(col.rgb.y < _boundary){
					col.rgb.y = _black.y;
				} else {
					col.rgb.y = _white.y;
				}
				if(col.rgb.z < _boundary){
					col.rgb.z = _black.z;
				} else {
					col.rgb.z = _white.z;
				}

				
				//if((col.rgb.x + col.rgb.y + col.rgb.z) / 3.0 < _boundary ) {
				//	col.rgb = _black;
				//} else {
				//	col.rgb = _white;
				//}
				//col.rgb = _White;
				
				//col.rgb = 1 - col.rgb;
				
				return col;
			}
			ENDCG
		}
	}
}
