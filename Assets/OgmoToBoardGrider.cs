﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OgmoToBoardGrider : MonoBehaviour {

	public static void MakeFromOgmoLevel(OgmoLevel level, Board target) {
		int scale = 0;
		foreach (OgmoLayer layer in level.layers.Values) {
			if (layer.tileWidth != 0) {
				scale = layer.tileWidth;
				break;
			}
		}
		target.InitGrid(level.width / scale, level.height / scale);

		foreach (var layer in level.layers.Values) {
			if (layer.layerType == LayerType.GRID) {
				for (int x = 0; x < layer.tiles.GetLength(0); x++) {
					for (int y = 0; y < layer.tiles.GetLength(1); y++) {
						if (layer.tiles[x, y] == 1) {
							target.Induct(new Point(x, y), OgmoToObjectMap.GetGrid(layer));
						}
					}
				}
			} else if (layer.layerType == LayerType.TILES) {
				for (int x = 0; x < layer.tiles.GetLength(0); x++) {
					for (int y = 0; y < layer.tiles.GetLength(1); y++) {
						if (layer.tiles[x, y] != -1) {
							target.Induct(new Point(x, y), OgmoToObjectMap.GetTile(layer, layer.tiles[x, y]));
						}
					}
				}
			} else {//entities
				foreach(OgmoEntity entity in layer.entities) {//TODO: rotation
					target.Induct(new Point(entity.x / scale, entity.y / scale), OgmoToObjectMap.GetEntity(layer, entity.name));
				}
			}

		}

	}
}
