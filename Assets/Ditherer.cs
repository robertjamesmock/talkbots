﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ditherer : MonoBehaviour {
	public Material ditherMaterial;
	public Material offsetMaterial;
	public RenderTexture offsetTexture;
	public Texture2D holdTexture;
	public Texture2D screenTexture;
	public Texture2D showTexture;


	private void Awake() {
		holdTexture = new Texture2D(Camera.main.pixelWidth, Camera.main.pixelHeight);
		screenTexture = new Texture2D(Camera.main.pixelWidth, Camera.main.pixelHeight);
		showTexture = new Texture2D(Camera.main.pixelWidth, Camera.main.pixelHeight);
		offsetTexture = new RenderTexture(Camera.main.pixelWidth, Camera.main.pixelHeight, 16, RenderTextureFormat.ARGB32);
		offsetTexture.Create();
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination) {
		
		ditherMaterial.SetTexture("_OffsetsTex", holdTexture);
		Graphics.Blit(source, destination, ditherMaterial);
		screenTexture.ReadPixels(new Rect(0, 0, screenTexture.width, screenTexture.height), 0, 0);
		screenTexture.Apply();

		RenderTexture.active = source;
		showTexture.ReadPixels(new Rect(0, 0, screenTexture.width, screenTexture.height), 0, 0);
		showTexture.Apply();

		offsetMaterial.SetTexture("_RendTex", screenTexture);
		offsetMaterial.SetTexture("_OffsTex", holdTexture);
		Graphics.Blit(source, offsetTexture, offsetMaterial);//they aren't going to let me bullshit this either, huh? can't access renderTexture results I GUESS//https://forum.unity.com/threads/what-is-the-best-solution-for-passing-last-rendered-frame-to-shader.486997/
		RenderTexture.active = offsetTexture;
		holdTexture.ReadPixels(new Rect(0, 0, offsetTexture.width, offsetTexture.height), 0, 0);
		holdTexture.Apply();
	}
}
