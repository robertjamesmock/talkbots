﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {
	public Grid<BoardSpace> boardGrid;

	void Start() {


		StartCoroutine(LoadLevel("level1"));



	}

	IEnumerator LoadLevel(string levelName) {
		string levelPath = System.IO.Path.Combine(Application.streamingAssetsPath, levelName + ".oel");
		WWW www = new WWW(levelPath);

		yield return www;
		string ogmoString = System.Text.Encoding.UTF8.GetString(www.bytes);
		OgmoLevel ogmoLevel = new OgmoLevel(ogmoString, levelName);
		OgmoToBoardGrider.MakeFromOgmoLevel(ogmoLevel, this);

	}

	public void InitGrid(int width, int height) {
		boardGrid = new Grid<BoardSpace>(width, height);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				boardGrid[x, y] = new BoardSpace();
			}
		}
	}

	public void Induct(Point position, GridObject gridObject) {
		gridObject.board = this;
		gridObject.gridPosition = position;
		gridObject.transform.position = new Vector3(position.x, 0, position.y);
	}

	public void ChangePosition(GridObject obj, Point newPosition) {
		if (obj.gridPosition != GridObject.negaPoint) { SetSlot(null, obj.SlotType, obj.gridPosition); }
		SetSlot(obj, newPosition);
	}

	private void SetSlot(GridObject obj, Point newPosition) {
		//obj must be nonnull or we can't surmise which slot to change
		SetSlot(obj, obj.SlotType, newPosition);
	}

	private void SetSlot(GridObject obj, BoardSlotType type, Point newPosition) {
		if (type == BoardSlotType.space) { boardGrid[newPosition].space = obj; boardGrid[newPosition].floor?.NotifyOfColocation(obj); }
		else if (type == BoardSlotType.floor) { boardGrid[newPosition].floor = obj; boardGrid[newPosition].space?.NotifyOfColocation(obj); }
		else { throw new System.NotImplementedException("slot not implemented into induct yet"); }//DEV remove final else/if
	}

}



public class BoardSpace {
	public GridObject space;
	public GridObject floor;
	public FloorTile floorTile;

	public BoardSpace(GridObject space, GridObject floor, FloorTile floorTile) {
		this.space = space;
		this.floor = floor;
		//this.floorTile = floorTile;
	}

	public BoardSpace() { }
}