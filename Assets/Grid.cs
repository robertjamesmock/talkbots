﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Grid<T> {
    private T[,] gridArray;

    public Grid(int width, int height) {
        gridArray = new T[width, height];
    }

    public T this[Point point] {
        get { return this[point.x, point.y]; }
        set { this[point.x, point.y] = value; }
    }

    public T this[int x, int y] {
        get { return gridArray[x, y]; }
        set { gridArray[x, y] = value; }
    }

    public int width { get { return gridArray.GetLength(0); } }
    public int height { get { return gridArray.GetLength(1); } }

    public bool InGrid(Point point) {
        return point.x >= 0 && point.y >= 0 && point.x < width && point.y<height;
    }
}
public struct Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Point operator +(Point a, Point b) {
        return new Point(a.x + b.x, a.y + b.y);
    }

    public static Point operator -(Point a, Point b) {
        return new Point(a.x - b.x, a.y - b.y);
    }

	public static bool operator ==(Point a, Point b) {
		return a.x == b.x && a.y == b.y;
	}

	public static bool operator !=(Point a, Point b) {
		return !(a == b);
	}

	public override int GetHashCode() {
		return (x.ToString() + " " + y.ToString()).GetHashCode();
	}

	public override bool Equals(object obj) {
		return (Point?)this == (obj as Point?);//if obj is not a point, cast will make it null, and this will never be.
		
	}
}