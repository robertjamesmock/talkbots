﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public partial class OgmoToObjectMap {// : MonoBehaviour {

	//FUCK UNITY
	//THIS COMMENT SHALL SERVE AS THE GRAVESTONE OF A DESIGN FOR THIS FUNCTIONALITY THAT WAS ANY GOOD
	//AT EVERY TWIST AND TURN OF ATTEMPTS TO MAKE UNITY ALLOW ME TO EDIT THESE ASSOCIATIONS IN THE EDITOR, I WAS SPURNED
	//NO VERSION OF THIS DATA WAS ALLOWED TO BE GENERIC OR INHERITED AND SERIALIZABLE
	//NO VERSION OF A CUSTOM INSPECTOR COULD WORK WITH THESE OBJECTS AND SAVE
	//NO VERSION OF A CUSTOM EDITOR WINDOW COULD WRITE AND ALSO READ THE CONTENTS OF THIS OBJECT
	//I NOW BELIEVE THAT UNITY'S OBJECT STORAGE IS SIMPLY UNABLE TO SAVE A LIST OF A CUSTOM CLASS STORING DERIVED CLASSES
	//AND SO WE ARRIVE HERE, AT THIS GARBAGE. THIS CODE IS UNNECESSARY, BUT IT WILL BE ALLOWED TO WORK
	//UNITY, WHERE GOOD CODE IS BANNED
	//UNITY, WHERE DOING IT YOURSELF IS BOTH NECESSARY AND IMPOSSIBLE
	//UNITY, WHERE MAKING A VIDEO GAME IS A SIDE-EFFECT BENEFITING ONLY THE NON-TARGET-AUDIENCE.
	public static GridObject GetTile(OgmoLayer layer, int index) {
		if (layer.tileSet == "flobjects") {
			if (index == 0) {
				return Factory.Spawn<Target>();
			} else if (index == 1) {
				return Factory.Spawn<Lever>();
			}
		} else if (layer.tileSet == "ununique objects") {
			if (index == 0) {
				return Factory.Spawn<Crate>();
			} else if (index == 1) {
				return Factory.Spawn<MetalCrate>();
			}
		}
		Debug.LogError("could not find tile: "+layer.tileSet+" "+index);
		return Factory.MakeMissing();
	}


	public static GridObject GetGrid(OgmoLayer layer) {
		if (layer.name == "walls"){
			return Factory.Spawn<Wall>();
		}
		Debug.LogError("could not find grid: " + layer.name);
		return Factory.MakeMissing();
	}

	public static GridObject GetEntity(OgmoLayer layer, string entityname) {
		if (entityname == "Player") {
			return Factory.Spawn<Player>();
		} else if (entityname == "robut") {
			return Factory.Spawn<MoveBot>();
		}
		Debug.LogError("could not find entity: " + layer.name + " " + entityname);
		return Factory.MakeMissing();
	}


}