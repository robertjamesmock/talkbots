﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : GridObject {



    public bool TryMove(Point direction){
        Point dest = gridPosition + direction;
        if (!board.boardGrid.InGrid(dest)){
            return false;//fail to move. Face wall though?    
        }
       if (board.boardGrid[dest].space!=null) {
            if (CanPush(board.boardGrid[dest].space)){
                if (((Mover)board.boardGrid[dest].space).TryMove(direction)) {
                    DoMove(direction);
                    return true;
                }
            }
            return false;
        } else {
            DoMove(direction);
            return true;
        }
    }

    void DoMove(Point direction) {
        //animate
        gridPosition += direction;
        transform.position = new Vector3(gridPosition.x,0, gridPosition.y);
    }

    bool CanPush(GridObject other) {
        if (other is Crate) {
            return true;
        }
        return false;
    }
}
