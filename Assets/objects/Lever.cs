﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : GridObject {

	public override BoardSlotType SlotType {
		get {
			return BoardSlotType.floor;
		}
	}
}
