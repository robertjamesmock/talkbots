﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour {
	public static readonly Point negaPoint = new Point(-1, -1);
	Point _gridPosition = negaPoint;
	public virtual BoardSlotType SlotType {
		get { return BoardSlotType.space; }
	}

    public Point gridPosition {
        get { return _gridPosition; }
        set { board.ChangePosition(this, value); _gridPosition = value; }
    }
    public Board board;

	public virtual void NotifyOfColocation(GridObject other) {

	}
}

public enum BoardSlotType {
	space,
	floor
	//,tile
}
