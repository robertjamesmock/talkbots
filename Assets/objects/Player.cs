﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Mover {

    public void Update() {
        if (Sinput.GetButtonDown("Left")) {
            
            TryMove(new Point(-1, 0));
        }
        if (Sinput.GetButtonDown("Right")) {
            TryMove(new Point(1, 0));
        }
        if (Sinput.GetButtonDown("Down")) {
            TryMove(new Point(0, -1));
        }
        if (Sinput.GetButtonDown("Up")) {
            TryMove(new Point(0, 1));
        }
    }
}
