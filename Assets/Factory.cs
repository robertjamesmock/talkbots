﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Factory : MonoBehaviour {

	public GameObject missingPrefab;

	public static Factory instance;

	private void Awake() {
		instance = this;
	}

	public static GridObject MakeMissing() {//because this was showing up in tab complete before Spawn >:[
		return Instantiate(instance.missingPrefab).GetComponent<GridObject>();
	}

	public static Player SpawnPlayer() {
		return Spawn<Player>();
	}

	public GameObject[] spawnObjects;

	public static T Spawn<T>() {
		return instance.ActuallySpawn<T>();
	}

	private T ActuallySpawn<T>() {
		if (availableObjects.ContainsKey(typeof(T)) && availableObjects[typeof(T)].Count > 0) {
			GameObject obj = availableObjects[typeof(T)].Dequeue();
			obj.SetActive(true);
			return obj.GetComponent<T>();
		}
		foreach (GameObject obj in spawnObjects) {
			if (obj.GetComponent<T>() != null) {
				return Instantiate(obj).GetComponent<T>();
			}
		}
		throw new System.Exception("Object tried to spawn not in spawn list: "+typeof(T).ToString());
	}

	public void Return(GameObject obj, System.Type type/*shit*/) {
		obj.SetActive(false);
		availableObjects[type].Enqueue(obj);
	}

	Dictionary<System.Type, Queue<GameObject>> availableObjects = new Dictionary<System.Type, Queue<GameObject>>();
}
